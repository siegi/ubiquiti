**Ce script n'est pas joli, juste fonctionnel. ;-)**
Pour installer le contrôleur Unifi sur votre machine :

```
apt-get update
apt-get upgrade -y
apt-get install git-core ca-certificates -y
mkdir /root/git
cd /root/git
git clone https://framagit.org/siegi/ubiquiti.git
bash -x /root/git/ubiquiti/install.sh
```

Projet sous licence GPLv3
