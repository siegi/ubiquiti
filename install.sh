# Variables
PATH_UBIQUITI=/root/git/ubiquiti

# Ajout du dépôt Ubiquiti
cat $PATH_UBIQUITI/etc/apt/sources.list >> /etc/apt/sources.list

# Ajout de la clé GPG de Ubiquiti
apt-key adv --keyserver keyserver.ubuntu.com --recv 06E85760C0A52C50

# Mise à jour des paquets
apt-get update

# Installation du contrôleur Unifi
apt-get install unifi -y
